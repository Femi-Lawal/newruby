Rails.application.routes.draw do
  get 'sessions/new'

  get 'users/new'

  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/about',   to: 'static_pages#about'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get  '/chat',    to: 'static_pages#chat'
  post 'messages', to: 'messages#create'  
  resources :users do
    member do
      get :following, :followers
    end
  end


  resources :microposts,          only: [:create, :destroy]  
  resources :relationships,       only: [:create, :destroy]  
  root 'static_pages#home'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
